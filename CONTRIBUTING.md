# Contributing to RedViking Software core Projects

:+1::tada: First off, thanks for taking the time to contribute! :tada::+1:

The following is a set of guidelines for contributing to RedViking's Software
core and packages. These are just guidelines, not rules, use your best 
judgment and feel free to propose changes to this document in a merge request.

This project adheres to the [Contributor Covenant 1.2](http://contributor-covenant.org/version/1/2/0).
By participating, you are expected to uphold this code. Please report 
unacceptable behavior to [dbrown@redviking.com](mailto:dbrown@redviking.com).

#### Table Of Contents

* [Submitting Issues](#submitting-issues)
* [Merge Requests](#merge-requests)
* [Git Commit Messages](#git-commit-messages)
* [Documentation Styleguide](#documentation-styleguide)
* [Issue and Merge Request Labels](#issue-and-merge-request-labels)

## Submitting Issues

* Before creating issues please read the notes below on debugging and submitting
  issues, and include as many details as possible with your report.
* Include the version of the project you are using and the OS.
* Include screenshots and animated GIFs whenever possible; they are immensely
  helpful.
* Include the behavior you expected and other places you've seen that behavior
  such as Emacs, vi, Xcode, etc.
* If you can reproduce the error, please do so to get the
  full stack trace and include it in the issue.
* Please setup a profile picture in GitLab to make yourself recognizable and 
  so we can all get to know each other better.

## Merge Requests

* Include screenshots and animated GIFs in your merge request whenever possible.
* Follow the [JavaScript](https://github.com/styleguide/javascript),
  and [CSS](https://github.com/styleguide/css) styleguides.
* Document new code based on the
  [Documentation Styleguide](#documentation-styleguide)
* End files with a newline.
* Place requires in the following order:
    * Built in Node Modules (such as `path`)
    * Local Modules (using relative paths)
* Avoid platform-dependent code:
    * Use `require('fs-plus').getHomeDirectory()` to get the home directory.
    * Use `path.join()` to concatenate filenames.
    * Use `os.tmpdir()` rather than `/tmp` when you need to reference the
      temporary directory.
* Using a plain `return` when returning explicitly at the end of a function.
    * Not `return null`, `return undefined`, `null`, or `undefined`

## Git Commit Messages

* Use the present tense ("Add feature" not "Added feature")
* Use the imperative mood ("Move cursor to..." not "Moves cursor to...")
* Limit the first line to 72 characters or less
* Reference issues and merge requests liberally
* Consider starting the commit message with an applicable emoji:
    * :art: `:art:` when improving the format/structure of the code
    * :racehorse: `:racehorse:` when improving performance
    * :non-potable_water: `:non-potable_water:` when plugging memory leaks
    * :memo: `:memo:` when writing docs
    * :penguin: `:penguin:` when fixing something on Linux
    * :apple: `:apple:` when fixing something on Mac OS
    * :checkered_flag: `:checkered_flag:` when fixing something on Windows
    * :bug: `:bug:` when fixing a bug
    * :fire: `:fire:` when removing code or files
    * :green_heart: `:green_heart:` when fixing the CI build
    * :white_check_mark: `:white_check_mark:` when adding tests
    * :lock: `:lock:` when dealing with security
    * :arrow_up: `:arrow_up:` when upgrading dependencies
    * :arrow_down: `:arrow_down:` when downgrading dependencies
    * :shirt: `:shirt:` when removing linter warnings


## Documentation Styleguide

* Use [Markdown](https://daringfireball.net/projects/markdown).

## Issue and Merge Request Labels

This section lists the labels we use to help us track and manage issues and pull
requests. Most labels are used across all RedViking Software repositories.

The labels are loosely grouped by their purpose, but it's not required that 
every issue have a label from every group or that an issue can't have more than 
one label from the same group.

Please open an issue on [RedViking Software Knowledge Base](https://git.redviking.com/red-viking/knowledge-base/issues) 
if you have suggestions for new labels, and if you notice some labels are 
missing on some repositories, then please open an issue on that repository.

#### Type of Issue and Issue State

| Label name | Description |
| --- | --- |
| `enhancement` | Feature requests. |
| `bug` | Confirmed bugs or reports that are very likely to be bugs. |
| `question` | Questions more than bug reports or feature requests (e.g. how do I do X). |
| `feedback` | General feedback more than bug reports or feature requests. |
| `help-wanted` | The RedViking Software core team would appreciate help from others in resolving these issues. |
| `beginner` | Less complex issues which would be good first issues to work on for users who want to contribute to RedViking Sofware core. |
| `more-information-needed` | More information needs to be collected about these problems or feature requests (e.g. steps to reproduce). |
| `needs-reproduction` | Likely bugs, but haven't been reliably reproduced. |
| `blocked` | Issues blocked on other issues. |
| `duplicate` | Issues which are duplicates of other issues, i.e. they have been reported before. |
| `wontfix` | The RedViking Software core team has decided not to fix these issues for now, either because they're working as intended or for some other reason. |
| `invalid` | Issues which are't valid (e.g. user errors). |
| `wrong-repo` | Issues reported on the wrong repository |

#### Topic Categories

| Label name | Description |
| --- | --- | --- | --- |
| `windows` | Related to project running on Windows. |
| `linux` | Related to project running on Linux. |
| `mac` | Related to project running on OSX. |
| `documentation` | Related to any type of documentation |
| `performance` | Related to performance. |
| `security` | Related to security. |
| `ui` | Related to visual design. |
| `api` | Related to project's public APIs. |
| `uncaught-exception` | Issues about uncaught exceptions |
| `crash` | Reports of project completely crashing. |
| `auto-indent` | Related to auto-indenting text. |
| `encoding` | Related to character encoding. |
| `network` | Related to network problems or working with remote files (e.g. on network drives). |
| `git` | Related to Git functionality (e.g. problems with gitignore files or with showing the correct file status). |

#### Core Team Project Management

| Label name | Description |
| --- | --- |
| `in-progress` | Tasks which the RedViking Software core team is working on currently. |
| `on-deck` | Tasks which the RedViking Software core team plans to work on next. |
| `shipping` | Tasks which the RedViking Software core team completed and will be released in one of the next releases. |
| `post-1.0-roadmap` | The RedViking Software core team's roadmap post version 1.0.0. |
| `red-viking` | Topics discussed for prioritization at the next meeting of RedViking Software core team members. |

#### Merge Request Labels

| Label name | Description
| --- | --- | --- | --- |
| `work-in-progress` | Merge requests which are still being worked on, more changes will follow. |
| `needs-review` | Merge requests which need code review, and approval from maintainers or RedViking Software core team. |
| `under-review` | Merge requests being reviewed by maintainers or RedViking Software core team. |
| `requires-changes` | Merge requests which need to be updated based on review comments and then reviewed again. |
| `needs-testing` | Merge requests which need manual testing. |