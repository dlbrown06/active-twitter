/**
 * Created by dbrown on 9/8/2015.
 */

'use strict';

/**
 * Module dependencies.
 */
var winston = require('winston'),
    fs = require('fs-extra'),
    path = require('path');

fs.ensureDirSync(path.join(path.resolve('./'), 'logs'));

var customLevels = {
    levels: {
        debug: 0,
        info: 1,
        warn: 2,
        error: 3
    },
    colors: {
        debug: 'green',
        info: 'blue',
        warn: 'yellow',
        error: 'red'
    }
};

winston.remove(winston.transports.Console);
winston.add(winston.transports.Console, {
    silent: (process.env.NODE_ENV === 'production'),
    level: 'debug',
    levels: customLevels.levels,
    colorize: true,
    timestamp: true,
    prettyPrint: true,
    handleExceptions: true
});

winston.add(winston.transports.File, {
    level: 'info',
    silent: (process.env.NODE_ENV === 'development'),
    colorize: true,
    timestamp: true,
    filename: path.join(path.resolve('./'), 'logs', require('../package.json').name + '.log'),
    maxsize: '100000000', //100MB
    maxFiles: 10,
    json: true,
    eol: "\n",
    prettyPrint: true,
    depth: null,
    showLevel: true,
    tailable: true,
    zippedArchive: true,
    handleExceptions: true
});

// make winston aware of your awesome colour choices
winston.addColors(customLevels.colors);
winston.emitErrs = true;

module.exports = winston;