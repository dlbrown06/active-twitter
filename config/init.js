'use strict';

module.exports = function() {

    var exit = require('exit');

    /**
     * display stack trace on app failure
     */
    process.on('uncaughtException', function(err) {
        console.error('Caught exception: ' + err);
        console.error('App Exited with a process.exit(1)', err.stack);
        exit(1);
    });

    /**
     * Setup the environment
     */
    require('./environment')();

    var log = require('./logger');
    log.info(require('../package.json').name + ' Application Initialized!');

};