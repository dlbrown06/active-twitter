'use strict';

module.exports = {
	paths: [
		'https://scotch.io/feed',
		'https://nodejs.org/en/feed/blog.xml',
		'http://blog.npmjs.org/rss'
	]
};