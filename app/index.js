'use strict';

var log = require('../config/logger'),
	twitter = require('./lib/twitter'),
	feeds = require('./lib/feeds'),
	_ = require('lodash'),
	async = require('async'),
	prompt = require('prompt'),
	moment = require('moment');

module.exports = function () {

	async.waterfall([

		//get articles from feeds
		function (callback) {
			feeds.getAllArticles(callback);
		},

		//require the user to choose whice article to tweet
		function (articles, callback) {

			var sortedArticles = _.sortByOrder(articles, 'date', 'desc');

			//build string to display
			var text = '\n\nHere are the 10 latest articles from your subscribed feeds.\nSelect the article that you would like to tweet about:\n\n';
			_.forEach(sortedArticles, function (article, key) {
				if (key > 9) {
					return;
				}

				text += '(' + (key + 1) + '): ' + moment(article.date).format('MMM DD, hh:mm a') + ' - ' + article.title + ' '  + article.link + '\n';
			});	

			text += '\n';

			var schema = {
			    properties: {
			      	articles: {
			      		description: text,
			        	pattern: /^[0-9]+$/,
			        	message: 'Article selection must be a number',
			        	required: true
			      	}
			    }
			};

			prompt.start();

			prompt.get(schema, function (err, result) {
				if (err) {
					return callback(err);
				}

			    var key = +_.values(result)[0] - 1;
			    if (sortedArticles[key]) {
			    	callback(null, sortedArticles[key]);	
			    } else {
			    	callback('Invalid article selection. Next time select the number beside the article title');	
			    }
			});	
		},

		//Give the user the ability to alter the tweet
		function (article, callback) {
		
			var schema = {
			    properties: {
			      	tweet: {
			      		description: '\nHere is your tweet:\n\n' + article.title + ' ' + article.link + '\n\nIf you\'re cool with that, just press enter.\nIf not, just write what you want, and I can append the link\n\n',
			      	}
			    }
			};

			prompt.start();

			prompt.get(schema, function (err, result) {
				if (err) {
					return callback(err);
				}

				if (result.tweet === '') {
					return callback(err, article.title + ' ' + article.link);
				} else {
					result.tweet += ' ' + article.link;
				}

				callback(err, result.tweet)
			});	
		},

		//tweet it
		function (tweet, callback) {
			if (+process.env.TWITTER_ENABLED) {
				twitter.statusUpdate(tweet, function (err) {
					if (!err) {
						log.info('Status \'' + tweet + '\' Posted!');
					}
				});	
			} else {
				log.warn('Posting \'' +  tweet + '\' to twitter is currently disabled');
			}
		} 
		

	], function (err) {
		if (err) {
			return log.error('Application Process Interrupted!', err);
		}

		log.info('Application Process Complete!');
	});

};