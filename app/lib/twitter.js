'use strict';

var log = require('../../config/logger'),
	twitter = require('twitter');

var Twitter = function () {
	this.client = new twitter({
		consumer_key: process.env.TWITTER_CONSUMER_KEY,
	  	consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
	  	access_token_key: process.env.TWITTER_ACCESS_KEY,
	  	access_token_secret: process.env.TWITTER_ACCESS_SECRET
	});
};

Twitter.prototype.statusUpdate = function (status, callback) {
	this.client.post('statuses/update', {status: status},  function(err){
	  	if(err) {
	  		log.warn('Error posting a status update to twitter: [' + err[0].code + '] ' + err[0].message, err);
	  		return callback(err);
	  	}

	  	return callback(null);
	});
};

module.exports = new Twitter();