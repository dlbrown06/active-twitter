'use strict';

var log = require('../../config/logger'),
	FeedParser = require('feedparser'),
	request = require('request'),
	async = require('async');

var Feeds = function () {
	
};

Feeds.prototype.getAllArticles = function (callback) {
	
	var articles = [];


	async.each(require('../../config/feeds').paths, function (feed, next) {

		var req = request(feed, {timeout: 10000, pool: false}),
  			feedparser = new FeedParser();

  		req.setMaxListeners(50);

  		// Some feeds do not respond without user-agent and accept headers.
  		// req.setHeader('user-agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36').setHeader('accept', 'text/html,application/xhtml+xml');

		req.on('error', function (error) {
			next(error);
		});

		req.on('response', function (res) {
		  	var stream = this;

		  	if (res.statusCode != 200) {
		  		return this.emit('error', new Error('Bad status code'));	
		  	} 

		  	stream.pipe(feedparser);
		});


		feedparser.on('error', function(error) {
		  	next(error);
		});

		feedparser.on('readable', function() {
		  	// This is where the action is!
		  	var stream = this,
		    	meta = this.meta, // **NOTE** the "meta" is always available in the context of the feedparser instance
		    	item;

			while (item = stream.read()) {
		  		log.debug('Feed: \'' + feed + '\' - Added article \'' + item.title || item.description + '\'');
		  		item.feedLink = feed;
		  		articles.push(item);
			}

		});

		feedparser.on('end', function () {
			log.info('Finished parsing the \'' + feed + '\' feed. ' + articles.length + ' articles added');
			next(null);
		});

	}, function (err) {
		if (err) {
			log.warn('Error gathering all articles from saved feeds', err);
		}

		return callback(err, articles);
	});

};

module.exports = new Feeds();